#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)
LDFLAGS2:=-Wl,-z,defs  -Wl,-as-needed -Wl,--no-undefined

configure: configure-stamp

configure-stamp:
	dh_testdir
	touch $@

build-arch: build-arch-stamp

build-arch-stamp: configure-stamp
	dh_testdir
		dh_auto_build --buildsystem=makefile --sourcedirectory=src -- -f Makefile.debian \
		'LD=$$(CXX)' \
		CFLAGS="$(CFLAGS) $(CPPFLAGS) -DDATADIR=\\\\\\\"/usr/share/games/nikwi\\\\\\\"" \
		CXXFLAGS="$(CXXFLAGS) $(CPPFLAGS) -DDATADIR=\\\\\\\"/usr/share/games/nikwi\\\\\\\"" \
		LDFLAGS="$(LDFLAGS) $(LDFLAGS2)"
	touch $@

build-indep: build-indep-stamp

build-indep-stamp: configure-stamp
	dh_testdir
	cd src/tools; make -f Makefile.debian \
		CFLAGS="$(CFLAGS) $(CPPFLAGS) -DDATADIR=\\\\\\\"/usr/share/games/nikwi\\\\\\\"" \
		CXXFLAGS="$(CXXFLAGS) $(CPPFLAGS) -DDATADIR=\\\\\\\"/usr/share/games/nikwi\\\\\\\"" \
		LDFLAGS="$(LDFLAGS) $(LDFLAGS2)"
	sh makedata.sh
	sh makepack.sh
	touch $@

build: configure-stamp build-arch-stamp build-indep-stamp

clean: configure-stamp
	dh_testdir
	dh_testroot
	rm -fv build-indep-stamp build-arch-stamp configure-stamp

	cd src; $(MAKE) clean -f Makefile.debian
	rm -rf data/objects data/tiles
	rm -fv src/nikwi/nikwi justdata.up

	dh_clean

install-arch: build-arch-stamp
	dh_testdir
	dh_testroot
	dh_prep -a
	dh_installdirs -a
	dh_install -a src/nikwi/nikwi usr/games
	dh_install -a debian/nikwi.png usr/share/icons/hicolor/64x64/apps
	dh_install -a debian/nikwi.desktop usr/share/applications
	dh_install -a manual usr/share/doc/nikwi
	find . -type d -empty -delete

install-indep: build-indep-stamp
	dh_testdir
	dh_testroot
	dh_prep -i
	dh_installdirs -i
	dh_install -i justdata.up usr/share/games/nikwi

install: build install-arch install-indep


# Build architecture-dependent files here.
binary-arch: install-arch
	dh_testdir
	dh_testroot
	dh_installchangelogs -a
	dh_installdocs -a
#	dh_install -a
	dh_installmenu -a
	dh_installman -a debian/nikwi.6
	dh_link -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

# Build architecture-independent files here.
binary-indep: install-indep
	dh_testdir
	dh_testroot
	dh_installchangelogs -i
	dh_installdocs -i
#	dh_install -i
	dh_link -i
	dh_strip -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_shlibdeps -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install install-indep \
	install-arch configure get-orig-source
